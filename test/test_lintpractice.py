import os, sys
sys.path.append(os.path.abspath('..'))
from src.lintpractice import Module

def test_is_even_func():
    assert Module.is_even(10) is True

def test_is_even_func2():
    assert Module.is_even(20) is True

def test_is_odd_func():
    assert Module.is_odd(20) is False
