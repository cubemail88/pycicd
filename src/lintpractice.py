"Math utils module"
class Module:
    "all method for Module Class"
    @classmethod
    def is_even(cls, num):
        "Return if num is even"
        return num % 2 == 0

    @classmethod
    def is_odd(cls, num):
        "Return if num is odd"
        return num % 2 != 0
